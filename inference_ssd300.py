#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# In[0]:
import os
import zmq
import argparse
import pyrealsense2 as rs
import cv2
import datetime
import numpy as np
from matplotlib import cm
from keras import backend as K
from models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras_layers.keras_layer_L2Normalization import L2Normalization
from ssd_encoder_decoder.ssd_output_decoder import decode_detections, decode_detections_fast

from tensorflow.python.client import device_lib
import json
CUDA_VISIBLE_DEVICES=-1


# see below: `y_pred` contains a fixed number of predictions per batch item, many of which are low-confidence predictions 
# or dummy entries. We therefore need to apply a confidence threshold to filter out the bad predictions. 
confidence_threshold=0.6 # Nominal: 0.75


def build_ssd(weights_path,img_height,img_width):
    from keras.optimizers import Adam
    
    nclasses=4
    classes = ['background', 'connector', 'electrolytic', 'inductor', 'ceramic']
    
    # Build the Keras model
    K.clear_session() # Clear previous models from memory.
        
    model = ssd_300(image_size=(img_height, img_width, 3),
                    n_classes=nclasses, # 4 nominal
                    mode='inference', # nominal: 'inference' or 'inference_fast'
                    l2_regularization=0.0005,
                    scales=[0.07, 0.15, 0.33, 0.51, 0.69, 0.87, 1.05], 
                    aspect_ratios_per_layer=[[1.0, 2.0, 0.5],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5],
                                             [1.0, 2.0, 0.5]],
                    two_boxes_for_ar1=True,
                    steps=[8, 16, 32, 64, 100, 300],
                    offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                    clip_boxes=False,
                    variances=[0.1, 0.1, 0.2, 0.2],
                    normalize_coords=True,
                    subtract_mean=[123, 117, 104],
                    swap_channels=[2, 1, 0],
                    confidence_thresh=confidence_threshold,
                    iou_threshold=0.45,
                    top_k=200,
                    nms_max_output_size=400)
    
    # Load the trained weights into the model.
    model.load_weights(weights_path, by_name=True)
    
    #Compile the model so that Keras won't complain the next time you load it.   
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    
    ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
    
    model.compile(optimizer=adam, loss=ssd_loss.compute_loss)
        
    print('Model built.')    
    return model, classes

# In[1]:
""" Set main parameters for the inference """
parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true')
parser.add_argument('--watchdog', action='store_true')
args = parser.parse_args()

# Set flags to tune the behaviour of the inference
# args.debug -> if true enable the real time display of predictions and further cmd line diagnostics. Nominal: False
    
# if true set a watchdog and stop execution after stopframe frames
if args.watchdog:
    stopframe=1000

# Load the trained weights from
weights_path = '/home/tera/t-brain/t-brain-inferencer/data/ssd300_tera.h5'
# Build a model using some pretrained weights
model, classes = build_ssd(weights_path,300,300)


# Set main parameters of the realsense pipeline
rsfps=30 # fps target for realsense camera. Nominal: 30

# Select the device that will run ANN computations. On a single GPU machine 0 runs on the local GPU-0 
# while 1 will use the system CPU
os.environ["CUDA_VISIBLE_DEVICES"]="-1" # nominal: "0"

# Print wich device has been selected
print('Environment configured. Inference running on:')
print(device_lib.list_local_devices())

# In[2]:
# Create an ipv4 (AF_INET) socket object using the tcp protocol (SOCK_STREAM)
#client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#
## Connect the client ((target, port))
#client.connect(('192.168.1.2', 1026))
#print('TCP socket initialized.')

# In[3]:
# Initialize realsense pipeline

# Configure color (and optionally the depth) streams
pipeline = rs.pipeline()
config = rs.config()

# config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30) # Nominal: 640x480 z16 30fps
camImgWidth = 1920
camImgHigh  = 1080

config.enable_stream(rs.stream.color, camImgWidth, camImgHigh, rs.format.bgr8, rsfps) # Nominal: 1920x18080 bgr8 30fps
# Start camera streaming
pipeline.start(config)
print('Camera pipeline started.')

# In[4]: 
""" Main """
print('Launching inference...')

# Acquire images from a camera or from a video
start_time = datetime.datetime.now()
num_frames = 0

#if args.debug:
#    # Create an empty cv2 window that will display the predictions
#    cv2.namedWindow('SSD viewer', cv2.WINDOW_NORMAL)

context = zmq.Context()
sock = context.socket(zmq.PUB)

sock.bind("tcp://127.0.0.1:6000")

def pub_message ( payload ) :
    # Message [prefix][message]
    message = payload
    sock.send_string( message )
    #print ( message )

TMCamPrev  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMCamPrev.npy' )
TMCamNet   = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMCamNet.npy'  )
TMCamWork  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMCamWork.npy' )

TMNetCam   = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMNetCam.npy' )
TMNetPrev  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMNetPrev.npy'  )
TMNetWork  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMNetWork.npy' )

TMPrevCam  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMPrevCam.npy' )
TMPrevNet  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMPrevNet.npy'  )
TMPrevWork = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMPrevWork.npy' )

TMWorkCam  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMWorkCam.npy' )
TMWorkNet  = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMWorkNet.npy'  )
TMWorkPrev = np.load( '/home/tera/t-brain/t-brain-inferencer/data/TMWorkPrev.npy' )

TMCamPrev
while True:
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()
    # depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()
    if not color_frame:
        continue
    # Convert images to numpy arrays
    image_np = np.asanyarray(color_frame.get_data())
    # depth_image = np.asanyarray(depth_frame.get_data())
#    image_np = cv2.flip(image_np, -1)
    image_np = cv2.flip(image_np, -1)
    try:
        imgBaseCamSpace = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    except:
        print("Error converting to RGB")
    imgBasePrevSpace = cv2.warpPerspective(imgBaseCamSpace, TMCamPrev, (1024, 1024))
    imgBaseNetSpace = cv2.warpPerspective(imgBaseCamSpace, TMCamNet, (300, 300))
    netInputImage = np.expand_dims(cv2.warpPerspective(image_np, TMCamNet, (300, 300)),0)
    y_pred = model.predict(netInputImage)
    y_pred_thresh = [y_pred[k][y_pred[k,:,1] > confidence_threshold] for k in range(y_pred.shape[0])]

    if args.debug:
        if ( len(y_pred_thresh[0]) > 0 ):
            np.set_printoptions(precision=2, suppress=True, linewidth=90)
            print("Predicted boxes:\n")
            print('   class   conf xmin   ymin   xmax   ymax')
            print(y_pred_thresh[0])
            print("--------------------------")

    if y_pred_thresh[0].shape[1] < 1:
        # If the model does not predict any high confidence predictions skip iteration
        pass

    else:
        # Visualize the predictions on the original full-resolution image
        if True:
            blob_id=0
            dataDict = {'items': []}
            for box in y_pred_thresh[0]:
                pNetSpace = (  (box[2]+box[4])/2  ,  (box[1]+box[3])/2  )
                xMinPrev = (box[2] / 300) * 1024
                yMinPrev = (box[3] / 300) * 1024
                xMaxPrev = (box[4] / 300) * 1024
                yMaxPrev = (box[5] / 300) * 1024
                pPrev = ((xMaxPrev+xMinPrev)/2,(yMaxPrev+yMinPrev)/2)
                color = cm.jet(int(box[0])) #colors[int(box[0])]
                label = ('{}: {:.2f}'.format(classes[int(box[0])], box[1]))
                cv2.rectangle(imgBasePrevSpace, (int(xMinPrev),int(yMinPrev)), (int(xMaxPrev), int(yMaxPrev)), (color), thickness=3,lineType=8)
                cv2.putText(imgBasePrevSpace, label, (int(xMinPrev), int(yMinPrev-10)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (color), 2)
                cv2.circle  ( imgBasePrevSpace , (int(pPrev[0])      , int(pPrev[1])), 10, (255, 0, 0), 3, 2)
                #cv2.putText ( imgBasePrevSpace , (int(pPrev[0])      , int(pPrev[0])), int(pPrev[1] + 10)), cv2.FONT_HERSHEY_SIMPLEX , 0.5, (color), 1)
                cv2.putText ( imgBasePrevSpace , str(blob_id) , (int(pPrev[0]-5), int(pPrev[1]-5)) , cv2.FONT_HERSHEY_SIMPLEX , 0.75, (color) , 2 )
                #cv2.line  (imgBasePrevSpace , (int(pPrev[0] + 10) , pPrev[1]), (pPrev[0] - 10, pPrev[1]), (255, 0, 0), 1, 1)
                #   cv2.line  (imgBasePrevSpace , (int(pPrev[0])      , pPrev[1] + 10), (pPrev[0], pPrev[1] - 10), (255, 0, 0), 1, 1)
                #   cv2.imshow('PrevSpace', cv2.cvtColor(imgBasePrevSpace, cv2.COLOR_RGB2BGR))
                dataDict["items"].append(
                    {'id': str(blob_id), 'class': str(int(box[0])), 'confidence': str(box[1]), 'xMinPrev': str(xMinPrev),
                     'yMinPrev': str(yMinPrev), 'xMaxPrev': str(xMaxPrev), 'yMaxPrev': str(yMaxPrev)})
                blob_id+=1
            pub_message( str(json.dumps ( dataDict , sort_keys=True , indent=4 ,  ensure_ascii=True)) )

#            # Send detected bboxes to the server using TCP
#            client.send(y_pred_thresh)
#            # Optionally check the server received data (buffer size: 4096)
#            response = client.recv(4096)

    # Calculate and display frames per second (FPS)
    num_frames += 1
    if args.debug:
        elapsed_time = (datetime.datetime.now() - start_time).total_seconds()
        fps='Frame number: '+str(num_frames)+' FPS: '+str(num_frames / elapsed_time-int(5))[0:5]
        cv2.putText(image_np, fps, (20, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (77, 255, 9), 2)                
        cv2.imshow('PrevSpace', cv2.cvtColor(imgBasePrevSpace, cv2.COLOR_RGB2BGR))
    else:
        if num_frames % 200 == 0:
            elapsed_time = (datetime.datetime.now() - start_time).total_seconds()
            fps='Running... Frame number: '+str(num_frames)+' FPS: '+str(num_frames / elapsed_time-int(5))[0:5]
            print(fps)
        elif num_frames==1:
            print('Running... Waiting for data to print diagnostics.')

    # Flush cv2 window if q key is pressed
    if cv2.waitKey(25) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break
    
    if args.watchdog:
        # Stop execution after stopframe (1000) frames
        if num_frames==stopframe: 
            pipeline.stop() # stop realsense pipeline and make the device available
#            client.shutdown() # notify the server before closing the client socket
#            client.close() # close TCP socket

            # Confirm graceful exit
            print('Graceful stop.')
            break
            
# Confirm exit
print('Stopped.')
