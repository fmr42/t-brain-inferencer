#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# In[0]:
import os
import zmq
import argparse
import pyrealsense2 as rs
import cv2
import datetime
import numpy as np
from matplotlib import cm
from keras import backend as K
from models.keras_ssd512 import ssd_512
from keras_loss_function.keras_ssd_loss import SSDLoss
from keras_layers.keras_layer_AnchorBoxes import AnchorBoxes
from keras_layers.keras_layer_DecodeDetections import DecodeDetections
from keras_layers.keras_layer_DecodeDetectionsFast import DecodeDetectionsFast
from keras_layers.keras_layer_L2Normalization import L2Normalization
from ssd_encoder_decoder.ssd_output_decoder import decode_detections, decode_detections_fast

from tensorflow.python.client import device_lib
import json
CUDA_VISIBLE_DEVICES=-1


# see below: `y_pred` contains a fixed number of predictions per batch item, many of which are low-confidence predictions 
# or dummy entries. We therefore need to apply a confidence threshold to filter out the bad predictions. 
confidence_threshold=0.25 # Nominal: 0.75

#classes = ['background', 'connector', 'electrolytic', 'inductor', 'ceramic']
classes = [ 'background' , 'gold' , 'silver' , 'hot' , 'cold' , 'impurity' , 'wave' , 'graphite' , 'good' ]

def build_ssd(weights_path,img_height,img_width):
    from keras.optimizers import Adam
    # TODO parametrize nclasses
    nclasses=len(classes)-1
    # Build the Keras model
    K.clear_session() # Clear previous models from memory.
        
    model = ssd_512(image_size=(img_height, img_width, 3),
                    n_classes=nclasses, # 4 nominal
                    mode='inference', # nominal: 'inference' or 'inference_fast'
                    l2_regularization=0.0005,
                    scales = [0.04, 0.1 , 0.26, 0.42, 0.58, 0.74, 0.9 , 1.06],
                    aspect_ratios_per_layer=[[1.0, 2.0, 0.5],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5, 3.0, 1.0/3.0],
                                             [1.0, 2.0, 0.5],
                                             [1.0, 2.0, 0.5]],
                    two_boxes_for_ar1=True,
                    steps=[8, 16, 32, 64, 128, 256, 512],
                    offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                    clip_boxes=False,
                    variances=[0.1, 0.1, 0.2, 0.2],
                    normalize_coords=True,
                    subtract_mean=[123, 117, 104],
                    swap_channels=[2, 1, 0],
                    confidence_thresh=confidence_threshold,
                    iou_threshold=0.45,
                    top_k=200,
                    nms_max_output_size=400)
    
    # Load the trained weights into the model.
    model.load_weights(weights_path, by_name=True)
    #Compile the model so that Keras won't complain the next time you load it.   
    adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
    model.compile(optimizer=adam, loss=ssd_loss.compute_loss)
    print('Model built.')    
    return model, classes

# Set main parameters for the inference """
parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true')
parser.add_argument('--watchdog', action='store_true')
args = parser.parse_args()
weights_path = '/home/tera/T-Brain/data/ssd512.h5'
model, classes = build_ssd(weights_path,512,512)

rsfps=30

# Select the device that will run ANN computations. On a single GPU machine 0 runs on the local GPU-0 
# while 1 will use the system CPU
os.environ["CUDA_VISIBLE_DEVICES"]="-1" # nominal: "0"

# Print wich device has been selected
print('Environment configured. Inference running on:')
print(device_lib.list_local_devices())

# In[3]:
# Initialize realsense pipeline

# Configure color (and optionally the depth) streams
pipeline = rs.pipeline()
config = rs.config()

# config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30) # Nominal: 640x480 z16 30fps
camImgWidth = 1920
camImgHigh  = 1080

config.enable_stream(rs.stream.color, camImgWidth, camImgHigh, rs.format.rgb8, rsfps) # Nominal: 1920x18080 bgr8 30fps
# Start camera streaming
pipeline.start(config)
print('Camera pipeline started.')

# In[4]: 
""" Main """
print('Launching inference...')

# Acquire images from a camera or from a video
start_time = datetime.datetime.now()
num_frames = 0

#if args.debug:
#    # Create an empty cv2 window that will display the predictions
#    cv2.namedWindow('SSD viewer', cv2.WINDOW_NORMAL)

context = zmq.Context()
socks = [ context.socket(zmq.PUB) ,
          context.socket(zmq.PUB) ,
          context.socket(zmq.PUB) ,
          context.socket(zmq.PUB) ,
          context.socket(zmq.PUB) ]
socks[0].bind("tcp://127.0.0.1:6000")
socks[1].bind("tcp://127.0.0.1:6001")
socks[2].bind("tcp://127.0.0.1:6002")
socks[3].bind("tcp://127.0.0.1:6003")
socks[4].bind("tcp://127.0.0.1:6004")

calib_base_dir = '/home/tera/T-Brain/data/calibration/'
TMCamPrev  = np.load( calib_base_dir + 'TMCamPrev.npy' )
TMCamNet   = np.load( calib_base_dir + 'TMCamNet.npy'  )
TMCamWork  = np.load( calib_base_dir + 'TMCamWork.npy' )

TMNetCam   = np.load( calib_base_dir + 'TMNetCam.npy' )
TMNetPrev  = np.load( calib_base_dir + 'TMNetPrev.npy'  )
TMNetWork  = np.load( calib_base_dir + 'TMNetWork.npy' )

TMPrevCam  = np.load( calib_base_dir + 'TMPrevCam.npy' )
TMPrevNet  = np.load( calib_base_dir + 'TMPrevNet.npy'  )
TMPrevWork = np.load( calib_base_dir + 'TMPrevWork.npy' )

TMWorkCam  = np.load( calib_base_dir + 'TMWorkCam.npy' )
TMWorkNet  = np.load( calib_base_dir + 'TMWorkNet.npy'  )
TMWorkPrev = np.load( calib_base_dir + 'TMWorkPrev.npy' )

colors = [ (   0,  0,  0 ) ,
           (   0,  0,255 ) ,
           (   0,255,  0 ) ,
           (   0,255,255 ) ,
           ( 255,  0,  0 ) ,
           ( 255,  0,255 ) ,
           ( 255,255,  0 ) ,
           ( 255,255,255 ) ]
frame_time_mean = 0
while True:
    print ("_______________________________________________________________________________________________________")
    t_0_start = datetime.datetime.now()
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()
    #depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()
    if not color_frame:
        continue
    # Convert images to numpy arrays
    # depth_image = np.asanyarray(depth_frame.get_data())
    image_np = np.asanyarray(color_frame.get_data())
    #image_np_depth = np.asanyarray(depth_frame.get_data())
#    image_np = cv2.flip(image_np, -1)
    image_np = cv2.flip(image_np, -1)
    #image_np_depth = cv2.flip(image_np_depth, -1)

    try:
        imgBaseCamSpace = cv2.cvtColor(image_np, cv2.COLOR_BGR2RGB)
    except:
        print("Error converting to RGB")
    t_1_acuisition = datetime.datetime.now()
    imgBasePrevSpace = cv2.warpPerspective(imgBaseCamSpace, TMCamPrev, (1024, 1024))
    imgBaseNetSpace =  cv2.resize(imgBasePrevSpace, (512, 512))
    netInputImage = np.expand_dims(imgBaseNetSpace,0)
    t_2_transform = datetime.datetime.now()
    y_pred = model.predict(netInputImage)
    print (len(y_pred))
    print ('-----------------------------------')
    print(y_pred)
    print ('-----------------------------------')

    y_pred_thresh = [y_pred[k][y_pred[k,:,1] > confidence_threshold] for k in range(y_pred.shape[0])]
    # TODO a loop is required here
    pred_byclass = [ np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 0].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 1].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 2].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 3].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 4].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 5].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 6].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 7].tolist()) ,
                     np.asarray(y_pred_thresh[0][y_pred_thresh[0][:, 0] == 8].tolist()) ]
    if True:
        #index_min = np.argmin(values)
        blob_id=0
        dataDict = [ {'items': []} ,
                     {'items': []} ,
                     {'items': []} ,
                     {'items': []} ,
                     {'items': []} ]
        #for i in range (len(classes)):
        #    if ( len(pred_byclass[i]) > 0 ) :
        #        box = pred_byclass[i][0]
        #        xMin = (box[2] / 512) * 1024
        #        yMin = (box[3] / 512) * 1024
        #        xMax = (box[4] / 512) * 1024
        #        yMax = (box[5] / 512) * 1024
        #        #cv2.rectangle(imgBasePrevSpace, (int(xMin)+10,int(yMin)+10), (int(xMax)-10, int(yMax)-10), colors[0] , thickness=4,lineType=8)
        #        #cv2.rectangle(imgBasePrevSpace, (int(xMin)+10,int(yMin)+10), (int(xMax)-10, int(yMax)-10), colors[i] , thickness=2,lineType=8)
        #        #cv2.rectangle(imgBasePrevSpace, (int(xMin)+20,int(yMin)+20), (int(xMax)-20, int(yMax)-20), colors[0] , thickness=3,lineType=8)
        #        #cv2.rectangle(imgBasePrevSpace, (int(xMin)+20,int(yMin)+20), (int(xMax)-20, int(yMax)-20), colors[i] , thickness=1,lineType=8)
        for c in range(len(classes)):
            pred_list = pred_byclass[c]
            print ("Predictions for " + classes[c].upper() )
            np.set_printoptions(precision=3, suppress=True, linewidth=90)
            print (pred_list)
            isFirstBox = True
            for box in pred_list:
                pNetSpace = (  (box[2]+box[4])/2  ,  (box[1]+box[3])/2  )
                xMin = (box[2] / 512) * 1024
                yMin = (box[3] / 512) * 1024
                xMax = (box[4] / 512) * 1024
                yMax = (box[5] / 512) * 1024
                pPrev = ((xMax+xMin)/2,(yMax+yMin)/2)
                label = ('{}: {:.3f}'.format(classes[int(box[0])].upper(), box[1]))
                if ( isFirstBox ):
                    #dataDict[c]["items"].append({'id': str(blob_id),
                    #                          'class': str(int(box[0])),
                    #                          'confidence': str(box[1]),
                    #                          'xMinPrev': str(xMin),
                    #                          'yMinPrev': str(yMin),
                    #                          'xMaxPrev': str(xMax),
                    #                          'yMaxPrev': str(yMax)})
                    cv2.line(imgBasePrevSpace, (int(xMin),int((yMin+yMax)/2)), (int(xMax),int((yMin+yMax)/2)), colors[0], thickness=6, lineType=cv2.LINE_AA, shift=0)
                    cv2.line(imgBasePrevSpace, (int((xMin+xMax)/2),int(yMin)), (int((xMin+xMax)/2),int(yMax)), colors[0], thickness=6, lineType=cv2.LINE_AA, shift=0)
                    cv2.line(imgBasePrevSpace, (int(xMin),int((yMin+yMax)/2)), (int(xMax),int((yMin+yMax)/2)), colors[c], thickness=2, lineType=cv2.LINE_AA, shift=0)
                    cv2.line(imgBasePrevSpace, (int((xMin+xMax)/2),int(yMin)), (int((xMin+xMax)/2),int(yMax)), colors[c], thickness=2, lineType=cv2.LINE_AA, shift=0)
                    isFirstBox=False
                    socks[c].send_string ( str(json.dumps ( dataDict[c] , sort_keys=True , indent=4 ,  ensure_ascii=True)) )
                cv2.rectangle(imgBasePrevSpace, (int(xMin),int(yMin)), (int(xMax), int(yMax)), colors[0] , thickness=6 , lineType=cv2.LINE_AA )
                cv2.rectangle(imgBasePrevSpace, (int(xMin),int(yMin)), (int(xMax), int(yMax)), colors[c] , thickness=2 , lineType=cv2.LINE_AA )
                cv2.putText(imgBasePrevSpace, label, (int(xMin), int(yMin-10)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, colors[0] , 3,lineType=cv2.LINE_AA)
                cv2.putText(imgBasePrevSpace, label, (int(xMin), int(yMin-10)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, colors[c] , 1,lineType=cv2.LINE_AA)
                cv2.circle  ( imgBasePrevSpace , (int(pPrev[0])      , int(pPrev[1])), 10, (255, 0, 0), 3 , 2 )
    cv2.imshow ( 'TERA AUTOMATION' , imgBasePrevSpace )
    frame_time = (datetime.datetime.now() - t_0_start).total_seconds()

    if ( frame_time_mean == 0 ):
        frame_time_mean = frame_time
    else:
        frame_time_mean = 0.9*frame_time_mean + 0.1*frame_time
    fps=1/frame_time
    fps_mean=1/frame_time_mean

    print("FPS: " + str(fps) )
    print("FPS mean: " + str(fps_mean) )

    # Flush cv2 window if q key is pressed
    key = cv2.waitKey(50) & 0xFF
    if key == ord('q') or key==27 : # pressing 'q' or ESC make the program exit
        break
   

cv2.destroyAllWindows()
print('Stopped.')

